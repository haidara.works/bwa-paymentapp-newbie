import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color whiteColor = Colors.white;

TextStyle titleTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.bold,
  fontSize: 26,
);
TextStyle subtitleTextStyle = GoogleFonts.poppins(
  color: Color(0xff899788),
  fontWeight: FontWeight.w300,
  fontSize: 16,
);
TextStyle descTextStyle = GoogleFonts.poppins(
  color: Colors.white,
  fontWeight: FontWeight.w300,
  fontSize: 12,
);
TextStyle planTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w500,
  fontSize: 14,
);
TextStyle priceTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w700,
  fontSize: 18,
);
TextStyle btnTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w500,
  fontSize: 16,
);
