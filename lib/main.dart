import 'package:flutter/material.dart';
import 'package:payment_app/theme.dart';

void main() => runApp(PaymentApp());

class PaymentApp extends StatefulWidget {
  @override
  State<PaymentApp> createState() => _PaymentAppState();
}

class _PaymentAppState extends State<PaymentApp> {
  int selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    Widget option(int index, String title, String subTitle, String Pricing) {
      return GestureDetector(
        onTap: () {
          setState(() {
            selectedIndex = index;
          });
        },
        child: Container(
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.all(20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            border: Border.all(
              color: selectedIndex == index
                  ? Color(0xff007DFF)
                  : Color(0xff4d5b7c),
            ),
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  selectedIndex == index
                      ? Image.asset(
                          'assets/radio_clicked.png',
                          width: 20,
                          height: 20,
                        )
                      : Image.asset(
                          'assets/radio.png',
                          width: 20,
                          height: 20,
                        ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: planTextStyle,
                      ),
                      Text(
                        subTitle,
                        style: descTextStyle,
                      )
                    ],
                  ),
                  SizedBox(
                    width: 91,
                  ),
                  Text(
                    Pricing,
                    style: priceTextStyle,
                  )
                ],
              )
            ],
          ),
        ),
      );
    }

    Widget header() {
      return Padding(
        padding: EdgeInsets.only(
          top: 50.0,
          left: 32,
          right: 32,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Image.asset(
                'assets/icon.png',
                width: 267,
                height: 200,
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 50,
                right: 50,
              ),
              child: Row(
                children: [
                  Text(
                    'Upgrade to ',
                    style: titleTextStyle,
                  ),
                  Text('Pro',
                      style: titleTextStyle.copyWith(
                        color: Color(0xff007DFF),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              'Go unlock all features and\nbuild your own business bigger',
              style: subtitleTextStyle,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      );
    }

    Widget checkout() {
      return Container(
        width: 311,
        height: 51.23,
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: Color(0xff007dff),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(71),
            ),
          ),
          onPressed: () {},
          child: Text(
            'Checkout Now',
            style: btnTextStyle,
          ),
        ),
      );
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xff04112f),
          body: SingleChildScrollView(
            child: Column(
              children: [
                header(),
                SizedBox(
                  height: 50,
                ),
                option(0, 'Monthly', 'Good for starting up', '\$20'),
                option(1, 'Quarterly', 'Focusing on building', '\$55'),
                option(2, 'Yearly', 'Steady company', '\$220'),
                SizedBox(
                  height: 50,
                ),
                selectedIndex == -1 ? SizedBox() : checkout(),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
